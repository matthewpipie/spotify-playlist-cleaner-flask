var data = {'successes': [], 'failures': []};
function updateColumns() {
	succ = document.getElementById('successes');
	fail = document.getElementById('failures');
	while (succ.firstChild) {
		succ.removeChild(succ.firstChild);
	}
	while (fail.firstChild) {
		fail.removeChild(fail.firstChild);
	}
	for (var i = 0; i < data['successes'].length; i++) {
		var text = data['successes'][i];
		if (text === "") continue;
		var entry = document.createElement('li');
		entry.appendChild(document.createTextNode(text));
		succ.insertBefore(entry, succ.firstChild);
	}

	for (var i = 0; i < data['failures'].length; i++) {
		var text = data['failures'][i];
		if (text === "") continue;
		var entry = document.createElement('li');
		entry.appendChild(document.createTextNode(text));
		fail.insertBefore(entry, fail.firstChild);
	}
	document.getElementById("succNum").innerHTML = succ.childElementCount
	document.getElementById("failNum").innerHTML = fail.childElementCount
}

function getUpdates(callback) {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '/spotify-playlist-cleaner/status?latestSuccessIndex=' + data['successes'].length + '&latestFailureIndex=' + data['failures'].length);
	xhr.onload = function() {
		console.log(xhr.responseText);
		console.log(xhr);
		oldData = data;
		data = JSON.parse(xhr.responseText);
		realSucc = [];
		for (var i = 0; i < data['successes'].length; i++) {
			if (data['successes'][i].length !== 0) {
				realSucc.push(data['successes'][i]);
			}
		}
		realFail = [];
		for (var i = 0; i < data['failures'].length; i++) {
			if (data['failures'][i].length !== 0) {
				realFail.push(data['failures'][i]);
			}
		}
		data['successes'] = oldData['successes'].concat(realSucc);
		data['failures'] = oldData['failures'].concat(realFail);
		updateColumns();
		callback()
	};
	xhr.send();
}

function autoUpdate() {
	if (!data['failed'] && (!data['clean-uri'] || data['clean-uri'] === null || data['clean-uri'].length === 0)) {
		getUpdates(function() {setTimeout(autoUpdate, 1000)});
	}
	else {
		if (data['failed']) {
			document.getElementById('doneText').innerHTML = "Error: Playlist cleaning failed. You probably entered a nonfunctional URI/URL in the previous step. Sorry!";
		}
		else {
			document.getElementById('doneText').innerHTML = "DONE! Check your spotify to find your clean playlist!";
		}
	}
}

document.addEventListener('DOMContentLoaded', function(){ 
	autoUpdate()
}, false);

