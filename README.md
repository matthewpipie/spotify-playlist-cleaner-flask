This is a tool that will clean your spotify playlists. It goes through each song and tries to find a clean version, then adds the clean version to a clean playlist that you own.

To use the tool, go there: [http://pipie.ddns.net/spotify-playlist-cleaner](http://pipie.ddns.net/spotify-playlist-cleaner)