#!/bin/bash
export FLASK_ENV=development
export FLASK_APP=server.py

flask run --host=0.0.0.0 --port=8001 > /dev/null 2>&1 &

while sleep 1; do python spotify-playlist-cleaner/dispatch.py; done&

