from flask import Flask
from flask import render_template
from flask import url_for
from flask import make_response
from flask import request,session,redirect
import json
import urllib
import subprocess
import os
import constants
app = Flask(__name__)

thisDir = os.path.dirname(os.path.abspath(__file__))
cleanerDir = os.path.join(thisDir, 'spotify-playlist-cleaner')
jobDir = os.path.join(cleanerDir, 'jobs')
submissionFile = os.path.join(cleanerDir, 'submitJob.py')

app.secret_key = constants.secretKey

def safe(string):
    return urllib.quote(string, safe='')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/block-schedule-helper/')
def BSH():
    return render_template('block-schedule-helper/index.html')
@app.route('/exocomets/')
def exocomets():
    return render_template('exocomets/index.html')
@app.route('/sjs/commserv/')
def commServ():
    return render_template('WIP.html')
@app.route('/sjs/review/')
def sjsReview():
    return render_template('WIP.html')
@app.route('/WIP')
def WIP():
    return render_template('WIP.html')

@app.route('/spotify-playlist-cleaner/')
def spotifyIndex():

    ensuranceHash = safe(os.urandom(32))
    authURL = r"https://accounts.spotify.com/authorize?client_id=" + constants.clientID + "&response_type=token&redirect_uri=" + safe(constants.domainBase + url_for('authCallback')) +\
    "&scope=" + safe(constants.scopes) + "&state=" + ensuranceHash
    resp = make_response(render_template('spotify/index.html', hrefUrl=authURL))
    resp.set_cookie('ensurance-hash', ensuranceHash)
    return resp

@app.route('/spotify-playlist-cleaner/auth-callback')
def authCallback():
    return render_template('spotify/auth-callback.html')

@app.route('/spotify-playlist-cleaner/submit', methods=["POST"])
def submit():
    uriToDo = request.form["uriInput"]
    token = request.form["token"]
    jobId = subprocess.check_output("python2 " + submissionFile + " " + uriToDo + " " + token, shell=True).strip()
    if "error" in jobId:
        return redirect(url_for('spotifyIndex'))
    session['jobId'] = jobId
    return redirect(url_for('processing'))
    #return redirect('/spotify-playlist-cleaner/processing')

@app.route('/spotify-playlist-cleaner/processing')
def processing():
    return render_template('spotify/processing.html')

@app.route('/spotify-playlist-cleaner/status', methods=["GET"])
def status():
    jobId = session['jobId']
    latestSuccessIndex = request.args.get("latestSuccessIndex")
    latestFailureIndex = request.args.get("latestFailureIndex")
    try:
        with open(os.path.join(jobDir, jobId)) as jobFile:
            #print jobFile.read()
            #jobFile.seek(0)
            try:
                ret = json.load(jobFile)['progress']
                hasToCut = False

                realLSI = int(latestSuccessIndex)
                realLFI = int(latestFailureIndex)

                ret['successes'] = ret['successes'][realLSI:]
                ret['failures'] = ret['failures'][realLFI:]
                while len(json.dumps(ret)) >= 4096:
                    ret['failures'] = ret['failures'][:-1]
                    ret['successes'] = ret['successes'][:-1]
                    hasToCut = True
                #print ret
                #print int(latestSuccessIndex)
                if hasToCut:
                    ret['clean-uri'] = ""

                return json.dumps(ret)
            except Exception:
                return json.dumps({'successes': [], 'failures': [], 'clean-uri': None, 'failed': jobFile['progress']['failed']})
    except Exception as e:
        return json.dumps({'successes': [], 'failures': [], 'failed': False, 'clean-uri': None})


if __name__ == "__name__":
	app.run()
